﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pipeline.ActionDependency;
using System.Linq;
using System.Collections.Generic;

namespace Pipeline.Test
{
    [TestClass]
    public class DynamicDependencyTests
    {
        [TestMethod]
        public void SimpleDependency()
        {
            List<string> data = Enumerable.Range(1, 10).Select(s => "output" + s).ToList();
            string result = data.Aggregate((x, y) => x + y);
            DynamicDependency dep = new DynamicDependency();
            string str = "";
            foreach (var item in data)
            {
                dep.PushVoidDependency(() =>
                {
                    str += item;
                });
            }
            dep.Execute();
            Assert.AreEqual(result, str);
        }
        [TestMethod]
        public void FailureTest()
        {
            DynamicDependency dep = new DynamicDependency();
            dep.StopOnFirstFailure = true;
            bool shouldNotbeSet = false;
            dep.PushVoidDependency(() => { int x = 0; int y = 0; int z = x / y; });
            dep.PushVoidDependency(() => shouldNotbeSet = true);
            dep.Execute();
            Assert.AreEqual(shouldNotbeSet, false);
        }
        [TestMethod]
        public void IgnoreDependencyExceptionTest()
        {
            DynamicDependency dep = new DynamicDependency();
            dep.StopOnFirstFailure = false;
            bool shouldbeSet = false;
            dep.PushVoidDependency(() => { int x = 0; int y = 0; int z = x / y; });
            dep.PushVoidDependency(() => shouldbeSet = true);
            dep.Execute();
            Assert.AreEqual(shouldbeSet, true);
        }
    }
}
