﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pipeline.ActionDependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.Test.Pipeline
{
    [TestClass]
    public class ValueDependencyTests
    {
        [TestMethod]
        public void BasicValueDependency()
        {
            ValueDependency<object> dep = new ValueDependency<object>();
            object outcome = null;
            dep.PushValueDependency(() => 5, 5, Enums.ActionPredicate.CompareValue);
            dep.PushValueDependency(() => outcome = new Object(), new Object(), Enums.ActionPredicate.CompareValue);
            dep.Execute();
            Assert.IsNotNull(outcome);
        }
        [TestMethod]
        public void BasicValueDependencyFailureTest()
        {
            ValueDependency<object> dep = new ValueDependency<object>();
            object outcome = null;
            dep.PushValueDependency(() => 7, 5, Enums.ActionPredicate.CompareValue);
            dep.PushValueDependency(() => outcome = new Object(), new Object(), Enums.ActionPredicate.CompareValue);
            dep.Execute();
            Assert.IsFalse(dep.DependencySucceeded);
        }
        [TestMethod]
        public void BasicValueDependencySkipOperationTest()
        {
            ValueDependency<object> dep = new ValueDependency<object>();
            object outcome = null;
            dep.SkipOnFirstFailure = true;
            dep.PushValueDependency(() => 7, 5, Enums.ActionPredicate.CompareValue);
            dep.PushValueDependency(() => outcome = new Object(), new Object(), Enums.ActionPredicate.CompareValue);
            dep.Execute();
            Assert.IsNull(outcome);
        }
    }
}
