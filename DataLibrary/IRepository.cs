﻿using Models;
using Models.DomainModels;
using Models.DomainModels.FactoryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataLibrary
{
    public interface IRepository<T> where T : IDistinguishedModel
    {
        Task<T> Add(T obj);
        T Update(T obj);
        bool Delete(T obj);
        bool DeleteMany(object[] ids);
        IEnumerable<T> Get(Expression<Func<T, bool>> expression);
        T GetSingle(Expression<Func<T, bool>> expression);
        IQueryable<T> Table { get; set; }

    }
}
