﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationManager.Interfaces
{
    public interface IApplicationBootstrapper : IDisposable
    {
        void Configure();
        void Configure(params object[] args);
    }
}
