﻿using Autofac;
using Autofac.Integration.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ConfigurationManager.Concrete.AutofacModules
{
    public class WebModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            

            // Register dependencies in controllers
            builder.RegisterControllers(typeof(WebUI.MvcApplication).Assembly);

            // Register dependencies in filter attributes
            builder.RegisterFilterProvider();

            // Register dependencies in custom views
            builder.RegisterSource(new ViewRegistrationSource());

            // Register our Data dependencies
            builder.RegisterModule(new CoreModule());

         
            base.Load(builder);
        }
     
    }
}
