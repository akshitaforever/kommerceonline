﻿using Autofac;
using Autofac.Integration.Mvc;
using ConfigurationManager.Concrete.AutofacModules;
using ConfigurationManager.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ConfigurationManager
{
    public class DefaultApplicationBootstrapper : IApplicationBootstrapper
    {
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new CoreModule());
            builder.RegisterModule(new WebModule());
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        public void Configure(params object[] args)
        {
            Configure();
        }
    }
}
