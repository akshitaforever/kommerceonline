﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFactory.SqlServer
{
    public class StoredProcInfo
    {
        public string Name { get; set; }
        public List<GenericDbParameter> Arguments { get; set; }
        public bool UseTransaction { get; set; }
        public bool LeaveConnectionOpen { get; set; }
      
        public StoredProcInfo()
        {
                
        }
        public StoredProcInfo(string Parameter)
        {
            this.Name = Parameter;
        }
        public string ConnectionKey { get; set; }
        public void AddParameter(string Parametername, Object Value)
        {
            Arguments = Arguments ?? new List<GenericDbParameter>();
            if (!Arguments.Any(p => p.ParameterName == Parametername))
            {
                Arguments.Add(new GenericDbParameter(Parametername, Value));
            }
            else { throw new InvalidOperationException("A Parameter with the same name has already been added"); }
            
        }
        public void RemoveParameter(string Parametername)
        {
            Arguments = Arguments ?? new List<GenericDbParameter>();
            var _element = Arguments.Single(p => p.ParameterName == Parametername);
            Arguments.Remove(_element);
        }
        
    }
}
