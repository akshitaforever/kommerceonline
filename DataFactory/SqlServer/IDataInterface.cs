﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFactory.SqlServer
{
    public interface IDataInterface
    {
         IEnumerable<DataRow> GetReversonatorData(string tld, out bool invalidTLD, bool showAll = false, string tldmlVersion = null, bool returnTldml = true, string tldLike = "");
         bool CreateData(string tld = "");

         bool UpdateData(string tldxml = "", string tld = "");

         string GenerateEmptyTemplate(int tldId, string tldName);



      
                //code added by hcl offshore team
         int GetTLDId(string tld);

         Object GetReversonatorMergeData(string section, int tldid);
         string GetDocXml(string tld, int docid);
    }
}
