﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataFactory.SqlServer.Helpers;
namespace DataFactory.SqlServer
{
    public class BaseDataResult
    {
        public List<GenericDbParameter> Arguments { get; set; }
        public Exception Error { get; protected set; }
        public bool IsSucceeded { get; set; }

        public Dictionary<string, Object> ParameterValues(ParameterDirection type)
        {

            Arguments = Arguments ?? new List<GenericDbParameter>();
            return Arguments.Where(p => p.Direction == type).ToDictionary(p => p.ParameterName, e => e.Value);
        }
        public Object GetReturnParameterValue()
        {

            return Arguments.First(p => p.Direction == ParameterDirection.ReturnValue).Value;
        }
        public Dictionary<string, Object> GetOutputParameterValues()
        {
            return Arguments.Where(p => p.Direction == ParameterDirection.Output || p.Direction == ParameterDirection.InputOutput).ToDictionary(p => p.ParameterName, e => e.Value);
        }
        protected virtual void SetError(Exception ex)
        {
            this.Error = ex;
            this.IsSucceeded = false;
        }

    }
    public class StoredProcQueryResult : BaseDataResult
    {




        public bool HasRows { get; protected set; }
        public DataSet Dataset { get; set; }
        public SqlException SqlError
        {
            get
            {
                return Error as SqlException;
            }
        }
        public StoredProcQueryResult(StoredProcInfo info, DataSet ds)
        {

            try
            {
                Arguments = info.Arguments;

                this.Dataset = ds;

            }
            catch (Exception ex)
            {
                SetError(ex);

            }

        }
        public StoredProcQueryResult(DataSet ds)
        {

            try
            {
                this.Dataset = ds;
                this.HasRows = ds.Tables.Count > 0;
                if (HasRows)
                    this.HasRows &= ds.Tables[0].Rows.Count > 0;
            }
            catch (Exception ex)
            {
                SetError(ex);

            }

        }
        protected override void SetError(Exception ex)
        {
            this.Error = ex;

            HasRows = false;
            IsSucceeded = false;
        }
        public StoredProcQueryResult(Exception ex)
        {
            SetError(ex);
        }
        public TupleEnumerator GetEnumerator()
        {
            return TupleEnumerator.Create(this.Dataset);
        }

    }
    public class StoredProcNonQueryResult : BaseDataResult
    {

        public int RecordsAffected { get; protected set; }
        public StoredProcNonQueryResult(int recordsaffected, StoredProcInfo info)
        {
            Arguments = info.Arguments;
            this.RecordsAffected = recordsaffected;
            this.IsSucceeded = recordsaffected > 0;
        }

        protected override void SetError(Exception ex)
        {
            this.Error = ex;
            RecordsAffected = 0;
            this.IsSucceeded = false;
        }
        public StoredProcNonQueryResult(Exception ex)
        {
            SetError(ex);
        }
        public StoredProcNonQueryResult(int recordsaffected)
        {
            this.RecordsAffected = recordsaffected;
            this.IsSucceeded = recordsaffected > 0;
        }
    }
    public class DataTuple
    {
        private DataRow ds = null;
        private DataTuple(DataRow ds)
        {
            this.ds = ds;
        }
        public static DataTuple Create(DataRow ds)
        {
            return new DataTuple(ds);
        }
        public string GetColumn(string column)
        {
            if (this.ds != null)
            {
                return this.ds[column].ToString();
            }
            return string.Empty;
        }
    }
    public class DataTuple<T> where T : class
    {
        private DataRow ds = null;
        private DataTuple(DataRow ds)
        {
            this.ds = ds;
        }
        public static DataTuple<T> Create(DataRow ds)
        {
            return new DataTuple<T>(ds);
        }
        public T GetColumn(string column)
        {
            if (this.ds != null)
            {
                return Convert.ChangeType(this.ds[column], typeof(T)) as T;
            }
            return default(T);
        }
    }
    public class TupleEnumerator : IEnumerator<DataTuple>
    {
        Queue<DataTuple> tuples = null;
        private DataSet ds = null;
        private DataTuple tuple = null;
        public DataTuple Current
        {
            get { return tuple; }
        }
        internal TupleEnumerator(DataSet ds)
        {
            this.ds = ds;
            LoadTuples();
        }
        public static TupleEnumerator Create(DataSet ds)
        {
            return new TupleEnumerator(ds);
        }
        public void Dispose()
        {
            tuples = null;
        }

        object System.Collections.IEnumerator.Current
        {
            get { return this; }
        }

        public bool MoveNext()
        {
            if (tuples.Count == 0)
                return false;
            tuple = tuples.Dequeue();
            return true;
        }

        public void Reset()
        {
            tuples.Clear();
            LoadTuples();
        }
        private void LoadTuples()
        {
            if (ds.HasData())
            {
                tuples = new Queue<DataTuple>(ds.Tables[0].Rows.Cast<DataRow>().Select(s => DataTuple.Create(s)));
            }
        }
    }
    public class TupleEnumerator<T> : IEnumerator<T> where T : class
    {
        Queue<T> tuples = null;
        private DataSet ds = null;
        private T tuple = null;
        public T Current
        {
            get { return tuple; }
        }
        internal TupleEnumerator(DataSet ds)
        {
            this.ds = ds;
            LoadTuples();
        }
        public static TupleEnumerator<T> Create(DataSet ds)
        {
            return new TupleEnumerator<T>(ds);
        }
        public void Dispose()
        {
            tuples = null;
        }

      

        public bool MoveNext()
        {
            if (tuples.Count == 0)
                return false;
            tuple = tuples.Dequeue();
            return true;
        }

        public void Reset()
        {
            tuples.Clear();
            LoadTuples();
        }
        private void LoadTuples()
        {
            if (ds.HasData())
            {
                tuples = new Queue<T>(ObjectMapper<T>.MapObject(ds));
            }
        }

      

        T IEnumerator<T>.Current
        {
            get { return tuple; }
        }

        object System.Collections.IEnumerator.Current
        {
            get { return tuple; }
        }
    }

}
