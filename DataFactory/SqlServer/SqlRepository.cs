﻿using DataFactory.SqlServer.Helpers;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataLibrary;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Common;
using Models.DomainModels;
using Models.DomainModels.FactoryModels;

namespace DataFactory.SqlServer
{
    public class SqlRepository<T> : ISqlRepository<T> where T : SqlBaseModel
    {
        DbContext _context = null;
        public SqlRepository()
        {

            _context = DependencyResolver.Current.GetService<DbContext>();

        }
        private DbContext GetContextForModel()
        {
            return _context;
            
        }
      
        public void ExecuteNonQueryFunction(string name, params DbParameter[] args)
        {

            ((IObjectContextAdapter)_context).ObjectContext.ExecuteFunction(name, args.ToObjectParameter());
        }
        public TReturn ExecuteQueryFunction<TReturn>(string name, params DbParameter[] args) where TReturn:class
        {
            return ((IObjectContextAdapter)_context).ObjectContext.ExecuteFunction<TReturn>(name, args.ToObjectParameter()) as TReturn;
            
        }
      
        public async Task<T> Add(T model)
        {

            GetContextForModel().Set<T>().Attach(model);
            GetContextForModel().SaveChanges();
            return model;
        }

        public T Update(T model)
        {
            var expression = StaticReflection.GetEntityExpression<T>(model);
            if (expression != null)
            {
                model = ObjectMapper<T>.MapObject(model, GetContextForModel().Set<T>().First(expression), true);
            }

            //  GetContextForModel().Set<T>().InsertOnSubmit(model);
            GetContextForModel().SaveChanges();
            return model;
        }

        public bool Delete(T model)
        {
            var expression = StaticReflection.GetEntityExpression<T>(model);
            if (expression != null)
            {
                model = ObjectMapper<T>.MapObject(model, GetContextForModel().Set<T>().First(expression), true);
            }
            GetContextForModel().Set<T>().Remove(model);
            GetContextForModel().SaveChanges();
            return true;
        }

        public T GetById(string idField, object Id)
        {
            return GetContextForModel().Set<T>().Single(PredicateHelper.GetExpression<T>(CollectionHelpers.GetDictionary(idField, Id)));
        }

        public IEnumerable<T> Get(System.Linq.Expressions.Expression<Func<T, bool>> expression, System.Linq.Expressions.Expression<Func<T, string>> orderby = null)
        {

            return GetAsQueryable(expression, orderby).AsEnumerable();
        }

        public T GetSingle(System.Linq.Expressions.Expression<Func<T, bool>> expression)
        {
            return GetContextForModel().Set<T>().Single(expression);
        }

        public IQueryable<T> GetAsQueryable(System.Linq.Expressions.Expression<Func<T, bool>> expression, System.Linq.Expressions.Expression<Func<T, string>> orderby = null)
        {
            var coll = GetContextForModel().Set<T>().Where(expression);
            if (orderby != null)
                coll = coll.OrderBy(orderby);
            return coll;
        }




       

        IQueryable<T> IRepository<T>.Table
        {
            get
            {
                return GetContextForModel().Set<T>().AsQueryable();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public bool DeleteMany(object[] ids)
        {
            var objs = GetContextForModel().Set<T>().Where(W => ids.Contains(W.ID));
            GetContextForModel().Set<T>().RemoveRange(objs);
            GetContextForModel().SaveChanges();
            return true;
        }

        public IEnumerable<T> Get(Expression<Func<T, bool>> expression)
        {
            return GetAsQueryable(expression).AsEnumerable();
        }
    }
}
