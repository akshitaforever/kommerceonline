﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataFactory.SqlServer
{
    public static class StaticReflection
    {
        public static string GetMemberName<T>(
            this T instance,
            Expression<Func<T, object>> expression)
        {
            return GetMemberName(expression);
        }

        public static string GetMemberName<T>(
            Expression<Func<T, object>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentException(
                    "The expression cannot be null.");
            }

            return GetMemberName(expression.Body);
        }

        public static string GetMemberName<T>(
            this T instance,
            Expression<Action<T>> expression)
        {
            return GetMemberName(expression);
        }

        public static string GetMemberName<T>(
            Expression<Action<T>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentException(
                    "The expression cannot be null.");
            }

            return GetMemberName(expression.Body);
        }

        private static string GetMemberName(
            Expression expression)
        {
            if (expression == null)
            {
                throw new ArgumentException(
                    "The expression cannot be null.");
            }

            if (expression is MemberExpression)
            {
                // Reference type property or field
                var memberExpression =
                    (MemberExpression)expression;
                return memberExpression.Member.Name;
            }

            if (expression is MethodCallExpression)
            {
                // Reference type method
                var methodCallExpression =
                    (MethodCallExpression)expression;
                return methodCallExpression.Method.Name;
            }

            if (expression is UnaryExpression)
            {
                // Property, field of method returning value type
                var unaryExpression = (UnaryExpression)expression;
                return GetMemberName(unaryExpression);
            }

            throw new ArgumentException("Invalid expression");
        }

        private static string GetMemberName(
            UnaryExpression unaryExpression)
        {
            if (unaryExpression.Operand is MethodCallExpression)
            {
                var methodExpression =
                    (MethodCallExpression)unaryExpression.Operand;
                return methodExpression.Method.Name;
            }

            return ((MemberExpression)unaryExpression.Operand)
                .Member.Name;
        }
        public static Expression<Func<T, bool>> GetEntityExpression<T>(Object value)
        {
            var primaryKeyField = typeof(T).GetProperties().Where(p => p.GetCustomAttributes(typeof(System.Data.Linq.Mapping.ColumnAttribute), true).Any())
               .Where(p => (p.GetCustomAttributes(typeof(System.Data.Linq.Mapping.ColumnAttribute), true).First() as System.Data.Linq.Mapping.ColumnAttribute).IsPrimaryKey);
            if (primaryKeyField.Any())
            {
                var field = primaryKeyField.First();
                ParameterExpression par1 = Expression.Parameter(typeof(T), "p1");
                //ParameterExpression par2 = Expression.Parameter(typeof(int), "p2");
                ConstantExpression ex1 = Expression.Constant(Convert.ChangeType(field.GetValue(value), field.PropertyType));
                MemberExpression ex2 = Expression.Property(par1, field);
                BinaryExpression expr = Expression.Equal(ex1, ex2);
                var f = Expression.Lambda<Func<T, bool>>(expr,
                                   new ParameterExpression[] { par1 });
                return f;
            }
            return null;
        }
    }
}
