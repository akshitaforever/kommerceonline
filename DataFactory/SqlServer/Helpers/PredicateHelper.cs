﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataFactory.SqlServer.Helpers
{
    public class PredicateHelper
    {
        public static Func<T, bool> GetExpression<T>(Dictionary<string, Object> values)
        {
            Func<T,bool> expression= (x) => {
                var enu = values.GetEnumerator();
                while (enu.MoveNext())
                {
                    if (Convert.ToString(typeof(T).GetProperty(enu.Current.Key).GetValue(x)) != Convert.ToString(enu.Current.Value))
                        return false;
                }
                return true;
            };
            return expression;
        }
    }
}
