﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFactory.SqlServer.Helpers
{
    public static class DataObjectHelpers
    {
        public static bool HasData(this DataSet ds)
        {
            if (ds == null)
                return false;
            if (ds.Tables.Count == 0)
                return false;
            if (ds.Tables[0].Rows.Count == 0)
                return false;
            return true;
        }
    }
}
