﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFactory.SqlServer.Helpers
{
    public class CollectionHelpers
    {
        public static Dictionary<string, Object> GetDictionary(string key, Object value)
        {
            List<KeyValuePair<string, Object>> objectData = new List<KeyValuePair<string, Object>>(){new KeyValuePair<string, Object>(key, value)};
            return objectData.ToDictionary(p => p.Key, s => s.Value);
        }
    }
}
