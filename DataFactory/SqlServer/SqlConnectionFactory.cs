﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFactory.SqlServer
{
    public class SqlDataObjectFactory
    {
        public static SqlConnection GetConnection(string connectionKey)
        {
            connectionKey = string.IsNullOrWhiteSpace(connectionKey) ? "Default1" : connectionKey;

            return new SqlConnection(ConfigurationManager.ConnectionStrings[connectionKey].ConnectionString);

        }
        public static SqlConnection GetConnection()
        {
            return GetConnection();
        }
        public static SqlCommand CreateCommand(SqlConnection conn, StoredProcInfo info)
        {


            SqlCommand cmd = new SqlCommand(info.Name, conn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddRange(info.Arguments.ToSqlParameters().ToArray());
            return cmd;

        }
        public static SqlCommand CreateCommand(SqlConnection conn, string cmdtext)
        {


            SqlCommand cmd = new SqlCommand(cmdtext, conn);

            cmd.CommandType = CommandType.Text;
            return cmd;

        }
        public static SqlCommand CreateCommandWithTransaction(SqlConnection conn, StoredProcInfo info, out SqlTransaction transaction)
        {

            var connection = GetConnection();
            SqlCommand cmd = new SqlCommand(info.Name, connection);
            transaction = connection.BeginTransaction(Guid.NewGuid().ToString());
            cmd.Transaction = transaction;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddRange(info.Arguments.ToSqlParameters().ToArray());
            return cmd;

        }
    }
}
