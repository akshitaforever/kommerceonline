﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataFactory.SqlServer
{
    public class ObjectMapper<T>
    {
        public static IEnumerable<T> MapObject(DataSet ds)
        {
            var listinstance = Activator.CreateInstance<List<T>>();
            var properties = typeof(T).GetProperties();
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                var instance = Activator.CreateInstance<T>();
                foreach (var property in properties.Where(p => ds.Tables[0].Columns.Contains(p.Name)))
                {
                    Type ValueType = property.PropertyType;

                    try
                    {
                        var value = Convert.ChangeType(item[property.Name], ValueType);
                        property.SetValue(instance, value);
                    }
                    catch (Exception ex)
                    {

                    }
                }
                listinstance.Add(instance);
            }
            return listinstance.AsEnumerable();
        }
        public static T MapObject(T src, T dest, bool overwrite)
        {
            var properties = typeof(T).GetProperties();
            foreach (var property in properties)
            {
                Type ValueType = property.PropertyType;

                try
                {
                    Object srcValue = property.GetValue(src);
                    Object destValue = property.GetValue(dest);
                    //src property is not null and dest is  null: replace
                    if (srcValue != null && destValue == null)
                    {
                        property.SetValue(dest, srcValue);
                    }
                    //src property is null and dest is not null: leave as is
                    //src property is null and dest property is null:no action required
                    //src and dest property have values:recursively merge values
                    if (srcValue != null && destValue != null)
                    {
                        //if (property.PropertyType.IsClass)
                        //{
                        //    property.SetValue(src, MapObject(property.PropertyType, src, dest, overwrite));
                        //}
                        //else
                        //{
                            if (overwrite)
                                property.SetValue(dest, srcValue);
                       // }
                    }

                }
                catch
                {

                }
            }
            return dest;
        }
        public static Object MapObject(Type type, Object src, Object dest, bool overwrite)
        {
            var properties = type.GetProperties();
            foreach (var property in properties)
            {


                try
                {
                    Object srcValue = property.GetValue(src);
                    Object destValue = property.GetValue(dest);
                    //src property is not null and dest is  null: replace
                    if (srcValue != null && destValue == null)
                    {
                        property.SetValue(dest, srcValue);
                    }
                    //src property is null and dest is not null: leave as is
                    //src property is null and dest property is null:no action required
                    //src and dest property have values
                    if (srcValue != null && destValue != null)
                    {
                        if (property.PropertyType.IsClass)
                        {
                            property.SetValue(src, MapObject(property.PropertyType, src, dest, overwrite));
                        }
                        else
                        {
                            if (overwrite)
                                property.SetValue(dest, srcValue);
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return dest;
        }
        public static T SetNullValue(Func<Object, bool> method, string path, T obj, Object replaceValue,int? ordinal=null)
        {
            Queue<string> pathQueue = new Queue<string>(path.Split('/'));
            Queue<string> targetQueue = new Queue<string>(path.Split('/'));
            SetPropertyByPath(typeof(T), obj, ref targetQueue, replaceValue);
            return obj;
        }
        private static KeyValuePair<PropertyInfo, Object> GetPropertyByPath(Type type, Object src, ref Queue<string> path)
        {
            var properties = type.GetProperties();
            var pname = path.Dequeue();
            Object curvalue = src;
            KeyValuePair<PropertyInfo, Object> dict = new KeyValuePair<PropertyInfo, object>();
            PropertyInfo currentprop = properties.SingleOrDefault(p => p.Name == pname);
            if (path.Count == 0)
            {
                curvalue = currentprop.GetValue(src);
                dict = new KeyValuePair<PropertyInfo, object>(currentprop, curvalue);
                return dict;
            }
            else
            {
                curvalue = currentprop.GetValue(src);
                var val = GetPropertyByPath(currentprop.PropertyType, curvalue, ref path);


                curvalue = val.Value;
                return val;
            }
        }
        public static PropertyInfo GetPropertyByPath(Type type, ref Queue<string> path)
        {
            var properties = type.GetProperties();
            var pname = path.Dequeue();
            //   Object curvalue = src;
            KeyValuePair<PropertyInfo, Object> dict = new KeyValuePair<PropertyInfo, object>();
            PropertyInfo currentprop = properties.SingleOrDefault(p => p.Name == pname);
            if (path.Count == 0)
            {
                //   curvalue = currentprop.GetValue(src);
                //   dict = new KeyValuePair<PropertyInfo, object>(currentprop, curvalue);
                //  return dict;
                return currentprop;
            }
            else
            {
                //   curvalue = currentprop.GetValue(src);
                var val = GetPropertyByPath(currentprop.PropertyType, ref path);
                //  curvalue = val.Value;
                return val;
            }
        }
        private static KeyValuePair<PropertyInfo, Object> SetPropertyByPath(Type type, Object src, ref Queue<string> path, Object replaceValue)
        {
            var properties = type.GetProperties();
            var pname = path.Dequeue();
            Object curvalue = src;
            KeyValuePair<PropertyInfo, Object> dict = new KeyValuePair<PropertyInfo, object>();
            PropertyInfo currentprop = properties.SingleOrDefault(p => p.Name == pname);
            if (path.Count == 0)
            {
                curvalue = currentprop.GetValue(src);
                if(currentprop.PropertyType.IsArray)
                {
                    if(curvalue==null)
                    {
                        //var ar = Array.CreateInstance(typeof)
                    }
                }
                currentprop.SetValue(src, replaceValue);
                dict = new KeyValuePair<PropertyInfo, object>(currentprop, replaceValue);
                return dict;
            }
            else
            {
                curvalue = currentprop.GetValue(src);
                var val = SetPropertyByPath(currentprop.PropertyType, curvalue, ref path, replaceValue);


                curvalue = val.Value;
                return val;
            }
        }
        private static void SetPropertyByPathtEMP(PropertyInfo property, Object src, ref Queue<string> path, Object replaceValue)
        {
            var properties = property.PropertyType.GetProperties();
            var pname = path.Dequeue();
            Object curvalue = src;
            KeyValuePair<PropertyInfo, Object> dict = new KeyValuePair<PropertyInfo, object>();
            PropertyInfo currentprop = properties.SingleOrDefault(p => p.Name == pname);
            if (path.Count == 0)
            {
                curvalue = currentprop.GetValue(src);
                dict = new KeyValuePair<PropertyInfo, object>(currentprop, curvalue);
            }
            else
            {
                curvalue = currentprop.GetValue(src);
                var val = GetPropertyByPath(currentprop.PropertyType, curvalue, ref path);
                curvalue = val.Value;
            }
        }
    }
}
