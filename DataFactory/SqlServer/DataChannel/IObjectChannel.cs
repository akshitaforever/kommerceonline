﻿using System;
namespace DataFactory.DataChannel
{
    public interface IObjectChannel<T>
    {
        void AddPostAction(IObjectActionBase<T> action, T obj);
        void AddPreAction(IObjectActionBase<T> action, T obj);
        void Execute(T obj, string method, params object[] param);
        TReturn Execute<TReturn>(T obj, string method, params object[] param) where TReturn :class;
        Action<Exception> OnException { get; set; }
    }
}
