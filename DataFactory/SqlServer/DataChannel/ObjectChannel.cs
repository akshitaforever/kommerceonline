﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;


namespace DataFactory.DataChannel
{
    public class ObjectChannel<T> : IObjectChannel<T>
    {

        private Dictionary<IObjectActionBase<T>, T> PreAction { get; set; }
        private Dictionary<IObjectActionBase<T>, T> PostAction { get; set; }
        public Action<Exception> OnException { get; set; }
        public void AddPreAction(IObjectActionBase<T> action, T obj)
        {
            PreAction = PreAction ?? new Dictionary<IObjectActionBase<T>, T>();
            this.PreAction.Add(action, obj);
        }
        public void AddPostAction(IObjectActionBase<T> action, T obj)
        {
            PostAction = PostAction ?? new Dictionary<IObjectActionBase<T>, T>();
            this.PostAction.Add(action, obj);
        }
        public void Execute(T obj, string method, params object[] param)
        {

            try
            {
                ExecutePreAction();
                typeof(T).GetMethod(method).Invoke(obj, param);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    OnException(ex.InnerException);
                else
                    OnException(ex);
            }
            finally
            {
                ExecutePostAction();
            }


        }
        private void ExecutePreAction()
        {
            var enu = PreAction.GetEnumerator();
            while (enu.MoveNext())
            {
                enu.Current.Key.Execute(enu.Current.Value);
            }
        }
        private void ExecutePostAction()
        {
            var enu = PostAction.GetEnumerator();
            while (enu.MoveNext())
            {
                enu.Current.Key.Execute(enu.Current.Value);
            }
        }
        public ObjectChannel()
        {
            PreAction = new Dictionary<IObjectActionBase<T>, T>();
            PostAction = new Dictionary<IObjectActionBase<T>, T>();
        }


        public TReturn Execute<TReturn>(T obj, string method, params object[] param) where TReturn : class
        {
            try
            {
                ExecutePreAction();
               return  typeof(T).GetMethod(method).Invoke(obj, param) as TReturn;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    OnException(ex.InnerException);
                else
                    OnException(ex);
            }
            finally
            {
                ExecutePostAction();
            }
            return null;
        }
    }

  
}
