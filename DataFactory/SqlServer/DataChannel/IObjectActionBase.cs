﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFactory.DataChannel
{
    public interface IObjectActionBase<T>
    {
        void Execute(T obj);
    }
    //test code
    public class LoggerAction<T> : IObjectActionBase<T>
    {
        public void Execute(T obj)
        {

        }
    }
}
