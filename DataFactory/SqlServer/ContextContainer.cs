﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFactory.SqlServer
{
    public class ContextContainer
    {
        IObjectContextAdapter qacontext = null;
        IObjectContextAdapter domaincontext = null;
        public ContextContainer()
        {
            qacontext = new DbContext("dummy1");
            domaincontext = new DbContext("dummy2");
        }
        public DbContext GetContextFor<T>()
        {
            if (domaincontext.ObjectContext.MetadataWorkspace.
                return domaincontext as DbContext;
            return qacontext;
        }
        public DbContext GetContextFor(string function)
        {
            if (domaincontext.Mapping.GetFunctions().Any(p => p.Name == function))
                return domaincontext;
            return qacontext;
        }
        public TContext GetContextFor<T,TContext>() where TContext: DbContext
        {
            if (domaincontext.Mapping.GetTables().Any(p => p.RowType.Type == typeof(T)))
                return domaincontext as TContext;
            return qacontext as TContext;
        }
        public TContext GetContextFor<T, TContext>(string function) where TContext : DbContext
        {
            if (domaincontext.Mapping.GetFunctions().Any(p => p.Name == function))
                return domaincontext as TContext;
            return qacontext as TContext;
        }
    }
}
