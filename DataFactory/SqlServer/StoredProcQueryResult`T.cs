﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFactory.SqlServer
{
    public class StoredProcQueryResult<T> : StoredProcQueryResult
    {

        public IEnumerable<T> Result { get; private set; }
        public StoredProcQueryResult(Exception ex)
            : base(ex)
        {
            SetError(ex);
        }
        public StoredProcQueryResult(StoredProcInfo info, DataSet ds)
            : base(info, ds)
        {

            try
            {

                Result = ObjectMapper<T>.MapObject(ds);
                this.HasRows = ds.Tables.Count > 0;
                if (HasRows)
                    this.HasRows &= ds.Tables[0].Rows.Count > 0;
            }
            catch (Exception ex)
            {
                SetError(ex);

            }

        }
        public StoredProcQueryResult(DataSet ds)
            : base(ds)
        {

            try
            {
                this.Dataset = ds;
                this.HasRows = ds.Tables.Count > 0;
                if (HasRows)
                    this.HasRows &= ds.Tables[0].Rows.Count > 0;
                Result = ObjectMapper<T>.MapObject(ds);
            }
            catch (Exception ex)
            {
                SetError(ex);

            }

        }
        protected override void SetError(Exception ex)
        {
            this.Error = ex;

            HasRows = false;
        }
        public TupleEnumerator<T> GetEnumerator<T>() where T:class
        {
            return TupleEnumerator<T>.Create(this.Dataset);
        }
    }
}
