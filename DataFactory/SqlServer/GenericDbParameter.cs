﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFactory.SqlServer
{
    public class GenericDbParameter
    {
        private string Parametername;
        private object Value1;

        public string ParameterName { get; set; }
        public ParameterDirection Direction { get; set; }
        public int Size { get; set; }
        public DbType DbType { get; set; }
        public Object Value { get; set; }
      
        public GenericDbParameter()
        {
                
        }
        public GenericDbParameter(string name, Object Value)
        {
            this.ParameterName = name;
            this.Value = Value;
        }
        public GenericDbParameter(string name, Object Value,int Size)
        {
            this.ParameterName = name;
            this.Value = Value;
            this.Size = Size;
        }
        public GenericDbParameter(string name, Object Value, int Size,DbType type)
        {
            this.ParameterName = name;
            this.Value = Value;
            this.Size = Size;
            this.DbType = type;
        }
        public GenericDbParameter(string name, Object Value, int Size, DbType type,ParameterDirection direction)
        {
            this.ParameterName = name;
            this.Value = Value;
            this.Size = Size;
            this.DbType = type;
            this.Direction = direction;
        }

      
    }
}
