﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFactory.SqlServer
{
    public static class SqlExtentions
    {
        public static SqlParameter ToOutputParameter(this SqlParameter param, SqlDbType type, int size)
        {
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = type;
            param.Size = size;
            return param;
        }
        public static SqlParameter ToVarcharOutputParameter(this SqlParameter param)
        {
            param.Direction = ParameterDirection.Output;
            param.SqlDbType = SqlDbType.VarChar;
            param.Size = 65536;
            return param;
        }
        public static SqlParameter ToReturnParameter(this SqlParameter param, SqlDbType type, int size)
        {
            param.Direction = ParameterDirection.ReturnValue;
            param.SqlDbType = type;
            param.Size = size;
            return param;
        }
        public static SqlParameter ToSqlParameter(this GenericDbParameter parameter)
        {
            return new SqlParameter()
            {
                ParameterName = parameter.ParameterName,
                Direction = parameter.Direction,
                Size = parameter.Size,
                DbType = parameter.DbType,
                Value=parameter.Value
            };
        }
        public static IEnumerable<SqlParameter> ToSqlParameters(this IEnumerable<GenericDbParameter> list)
        {
            return list.Select(s => s.ToSqlParameter());

        }
        public static IEnumerable<SqlParameter> ToSqlParameters(this List<GenericDbParameter> list)
        {
            return list.Select(s => s.ToSqlParameter());

        }
        public static ObjectParameter[] ToObjectParameter(this DbParameter[] obj)
        {
            return obj.Select(s => new ObjectParameter(s.ParameterName, s.Value) {
                
            }).ToArray();
        }
    }
}
