﻿using DataLibrary;
using Models;
using Models.DomainModels;
using Models.DomainModels.FactoryModels;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataFactory.SqlServer
{
    public interface ISqlRepository<T>:IRepository<T> where T : IDistinguishedModel
    {
        T GetById(string idField,object Id);
        IEnumerable<T> Get(Expression<Func<T,bool>> expression,Expression<Func<T,string>> orderby=null);
        IQueryable<T> GetAsQueryable(Expression<Func<T, bool>> expression, Expression<Func<T, string>> orderby=null);
        //Table<T> Table { get; }
        //void ExecuteNonQueryFunction<T1>(bool domaincontext, string name, T1 arg1);
        //void ExecuteNonQueryFunction<T1, T2>(bool domaincontext, string name, T1 arg1, T2 arg2);
        //void ExecuteNonQueryFunction<T1, T2, T3>(bool domaincontext, string name, T1 arg1, T2 arg2, T3 arg3);
        //void ExecuteNonQueryFunction<T1, T2, T3, T4>(bool domaincontext, string name, T1 arg1, T2 arg2, T3 arg3, T4 arg4);
        //void ExecuteNonQueryFunction<T1, T2, T3, T4, T5>(bool domaincontext, string name, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);
        TReturn ExecuteQueryFunction<TReturn>(string name, params DbParameter[] args) where TReturn : class;
        void ExecuteNonQueryFunction(string name, params DbParameter[] args);
    }
}
