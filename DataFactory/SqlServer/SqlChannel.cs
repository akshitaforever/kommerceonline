﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.Threading;
using DataFactory.SqlServer.Helpers;
namespace DataFactory.SqlServer
{
    public class SqlChannel<T> : IDbChannel<T>
    {
        private static Object _lockObject = new object();
        public StoredProcNonQueryResult EndInvokeNonQueryAsync(IAsyncResult result)
        {
            return (result.AsyncState as Func<StoredProcInfo, StoredProcNonQueryResult>).EndInvoke(result);
        }
        public StoredProcQueryResult EndInvokeQueryAsync(IAsyncResult result)
        {
            return (result.AsyncState as Func<StoredProcInfo, StoredProcQueryResult>).EndInvoke(result);
        }
        private delegate StoredProcQueryResult NonQueryMethod(StoredProcInfo info);
        private static SqlChannel<T> repo = new SqlChannel<T>();
        //private SqlRepository()
        //{

        //}
        public static SqlChannel<T> Instance
        {
            get
            {
                return repo;
            }
        }
        public static SqlChannel<T> ThreadSafeInstance
        {
            get
            {
                SpinLock lok = new SpinLock();
                bool success = false;
                try
                {


                    lok.TryEnter(ref success);

                    lock (_lockObject)
                    {
                        return repo;
                    }
                }
                catch (Exception)
                {


                }
                finally
                {
                    if (success) lok.Exit(false);
                }
                return null;
            }
        }
        public StoredProcQueryResult<T> ExecuteQueryProc(StoredProcInfo sqlinfo)
        {

            try
            {
                var connection = SqlDataObjectFactory.GetConnection(sqlinfo.ConnectionKey);
                connection.Open();
                var cmd = SqlDataObjectFactory.CreateCommand(connection, sqlinfo);
                connection.Close();
                return new StoredProcQueryResult<T>(sqlinfo, ExecuteDataSet(cmd));
            }
            catch (Exception ex)
            {
                return new StoredProcQueryResult<T>(ex);
            }

        }
        public StoredProcQueryResult<TReturn> ExecuteQueryProcAsModel<TReturn>(StoredProcInfo sqlinfo) where TReturn : class
        {

            try
            {
                var connection = SqlDataObjectFactory.GetConnection(sqlinfo.ConnectionKey);
                connection.Open();
                var cmd = SqlDataObjectFactory.CreateCommand(connection, sqlinfo);
                connection.Close();
                return new StoredProcQueryResult<TReturn>(sqlinfo, ExecuteDataSet(cmd));
            }
            catch (Exception ex)
            {
                return new StoredProcQueryResult<TReturn>(ex);
            }

        }
        private DataSet ExecuteDataSet(SqlCommand cmd)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
        public StoredProcNonQueryResult ExecuteNonQueryProc(StoredProcInfo sqlinfo)
        {
            try
            {



                var connection = SqlDataObjectFactory.GetConnection(sqlinfo.ConnectionKey);
                connection.Open();
                var cmd = SqlDataObjectFactory.CreateCommand(connection, sqlinfo);
                int records = cmd.ExecuteNonQuery();
                connection.Close();
                return new StoredProcNonQueryResult(records, sqlinfo);

            }
            catch (Exception ex)
            {
                return new StoredProcNonQueryResult(ex);
            }
        }


        public IAsyncResult ExecuteQueryProcAsync(StoredProcInfo sqlinfo, AsyncCallback callback, object stateobject = null)
        {
            Func<StoredProcInfo, StoredProcQueryResult<T>> method = (x) => ExecuteQueryProc(sqlinfo);
            return method.BeginInvoke(sqlinfo, callback, method);
        }

        public IAsyncResult ExecuteNonQueryProcAsync(StoredProcInfo sqlinfo, AsyncCallback callback, object stateobject = null)
        {
            Func<StoredProcInfo, StoredProcNonQueryResult> method = (x) => ExecuteNonQueryProc(sqlinfo);
            return method.BeginInvoke(sqlinfo, callback, method);
        }





        public StoredProcNonQueryResult ExecuteNonQueryProcInTransaction(StoredProcInfo sqlinfo)
        {
            var connection = SqlDataObjectFactory.GetConnection(sqlinfo.ConnectionKey);
            try
            {




                connection.Open();
                SqlTransaction transaction;
                var cmd = SqlDataObjectFactory.CreateCommandWithTransaction(connection, sqlinfo, out transaction);
                int records = cmd.ExecuteNonQuery();
                transaction.Commit();
                connection.Close();
                return new StoredProcNonQueryResult(records, sqlinfo);

            }
            catch (Exception ex)
            {
                return new StoredProcNonQueryResult(ex);
            }
            finally
            {
                connection.Close();
            }
        }


        public StoredProcQueryResult ExecuteRawQuery(string connectionkey, string cmdtext)
        {
            try
            {
                var connection = SqlDataObjectFactory.GetConnection(connectionkey);
                connection.Open();
                var cmd = SqlDataObjectFactory.CreateCommand(connection, cmdtext);
                connection.Close();
                return new StoredProcQueryResult(ExecuteDataSet(cmd));

            }
            catch (Exception ex)
            {
                return new StoredProcQueryResult(ex);
            }
        }
        public StoredProcNonQueryResult ExecuteRawCommand(string connectionkey, string cmdtext)
        {
            try
            {
                var connection = SqlDataObjectFactory.GetConnection(connectionkey);
                connection.Open();
                var cmd = SqlDataObjectFactory.CreateCommand(connection, cmdtext);
                int records = cmd.ExecuteNonQuery();
                connection.Close();
                return new StoredProcNonQueryResult(records);

            }
            catch (Exception ex)
            {
                return new StoredProcNonQueryResult(ex);
                throw new Exception(ex.Message);
            }
        }
        public StoredProcQueryResult<T> ExecuteRawQueryGeneric(string connectionkey, string cmdtext)
        {
            try
            {
                var connection = SqlDataObjectFactory.GetConnection(connectionkey);
                connection.Open();
                var cmd = SqlDataObjectFactory.CreateCommand(connection, cmdtext);
                return new StoredProcQueryResult<T>(ExecuteDataSet(cmd));

            }
            catch (Exception ex)
            {
                return new StoredProcQueryResult<T>(ex);
            }
        }








        public TReturn ExecuteScalerQueryProc<TReturn>(string column, StoredProcInfo sqlinfo)
        {
            var connection = SqlDataObjectFactory.GetConnection(sqlinfo.ConnectionKey);
            connection.Open();
            var cmd = SqlDataObjectFactory.CreateCommand(connection, sqlinfo);
            connection.Close();
            var ds = ExecuteDataSet(cmd);
            if (ds.HasData())
            {
                var data = ds.Tables[0].Rows[0][column];
                if (data == System.DBNull.Value)
                    return default(TReturn);
                return (TReturn)Convert.ChangeType(data, typeof(TReturn));
            }
            return default(TReturn);
        }





        public StoredProcQueryResult<TResult> ExecuteRawQueryAsModel<TResult>(string connectionkey, string cmdtext)
        {
            try
            {
                var connection = SqlDataObjectFactory.GetConnection(connectionkey);
                connection.Open();
                var cmd = SqlDataObjectFactory.CreateCommand(connection, cmdtext);
                connection.Close();
                return new StoredProcQueryResult<TResult>(ExecuteDataSet(cmd));

            }
            catch (Exception ex)
            {
                return new StoredProcQueryResult<TResult>(ex);
            }
        }


        public IEnumerable<TReturn> ExecuteAsScalerCollection<TReturn>(string column,StoredProcInfo sqlinfo) where TReturn:class
        {
            var connection = SqlDataObjectFactory.GetConnection(sqlinfo.ConnectionKey);
            
            connection.Open();
            var cmd = SqlDataObjectFactory.CreateCommand(connection, sqlinfo);
            connection.Close();
            var ds = ExecuteDataSet(cmd);
            if (ds.HasData())
            {
                return ds.Tables[0].Rows.Cast<DataRow>().Select(s => Convert.ChangeType((TReturn)s[column], typeof(TReturn)) as TReturn).ToList();
             
            }
            else
            {
                return Enumerable.Empty<TReturn>();
            }
            
        }
    }
}
