﻿using System;
using System.Collections.Generic;
using System.Data;
namespace DataFactory.SqlServer
{
    public interface IDbChannel<T>
    {
        StoredProcNonQueryResult ExecuteNonQueryProc(StoredProcInfo sqlinfo);
        StoredProcNonQueryResult ExecuteNonQueryProcInTransaction(StoredProcInfo sqlinfo);
        StoredProcQueryResult<T> ExecuteQueryProc(StoredProcInfo sqlinfo);
        TReturn ExecuteScalerQueryProc<TReturn>(string column, StoredProcInfo sqlinfo);
        IAsyncResult ExecuteQueryProcAsync(StoredProcInfo sqlinfo, AsyncCallback callback, Object stateobject = null);
        IAsyncResult ExecuteNonQueryProcAsync(StoredProcInfo sqlinfo, AsyncCallback callback, Object stateobject = null);
        StoredProcQueryResult ExecuteRawQuery(string connectionkey, string cmdtext);
        StoredProcQueryResult<TResult> ExecuteRawQueryAsModel<TResult>(string connectionkey, string cmdtext);
        StoredProcQueryResult<T> ExecuteRawQueryGeneric(string connectionkey, string cmdtext);
        StoredProcNonQueryResult ExecuteRawCommand(string connectionkey, string cmdtext);
        IEnumerable<TReturn> ExecuteAsScalerCollection<TReturn>(string column, StoredProcInfo sqlinfo) where TReturn : class;
        StoredProcQueryResult<TReturn> ExecuteQueryProcAsModel<TReturn>(StoredProcInfo sqlinfo) where TReturn : class;

    }
}
