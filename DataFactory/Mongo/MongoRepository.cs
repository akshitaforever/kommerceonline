﻿using DataLibrary;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using MongoDB.Driver;
using MongoDB.Bson;
using Models.DomainModels;
using Models.DomainModels.FactoryModels;

namespace DataFactory.Mongo
{
    public class MongoRepository<T> : IRepository<T> where T : MongoBaseModel
    {
        private static IMongoClient _client;
        private static IMongoDatabase _database;
        IMongoCollection<T> _data = null;
        public MongoRepository(string connectionstring,string db)
        {

            _client = new MongoClient(connectionstring);
            _database = _client.GetDatabase(db);
            _data = _database.GetCollection<T>(typeof(T).Name);
        }
        public IQueryable<T> Table
        {
            get
            {
                throw new NotImplementedException();

            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public async Task<T> Add(T obj)
        {
            try
            {
                await _data.InsertOneAsync(obj);
            }
            catch (Exception ex)
            {

                
            }
            return await _data.Find(p => p.IID == obj.IID).FirstAsync();
        }

        public bool Delete(T obj)
        {
            return _data.DeleteOneAsync(p => p.IID == obj.IID).GetAwaiter().GetResult().IsAcknowledged;
        }

        public bool DeleteMany(object[] ids)
        {
            var oids = ids.Select(s => (ObjectId)s);
            _data.DeleteManyAsync(prop => oids.Contains(prop.IID)).RunSynchronously();
            return true;
        }

        public IEnumerable<T> Get(Expression<Func<T, bool>> expression)
        {
            return _data.Find(expression).ToListAsync().GetAwaiter().GetResult();
        }

        public T GetSingle(Expression<Func<T, bool>> expression)
        {
            return _data.Find(expression).FirstAsync().GetAwaiter().GetResult();
        }

        public T Update(T obj)
        {
            if (_data.ReplaceOneAsync(prop => prop.IID == obj.IID, obj).GetAwaiter().GetResult().IsAcknowledged)
            {
                return GetSingle(p => p.IID == obj.IID);
            }
            return obj;

        }
    }
}
