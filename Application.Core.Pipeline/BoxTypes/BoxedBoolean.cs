﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.BoxTypes
{
    public class BoxedBoolean
    {
        private bool _value = false;
        public BoxedBoolean(bool initialvalue)
        {
            this._value = initialvalue;
        }
        public override string ToString()
        {
            return _value.ToString();
        }
        public bool GetValue()
        {
            return _value;
        }
        public static bool operator==(BoxedBoolean c1, BoxedBoolean c2)
        {
            return c1.GetValue() == c2.GetValue();
        }
        public static bool operator !=(BoxedBoolean c1, BoxedBoolean c2)
        {
            return c1.GetValue() != c2.GetValue();
        }
    }
}
