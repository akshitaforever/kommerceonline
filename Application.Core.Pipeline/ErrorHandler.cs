﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline
{
    public class ErrorHandler
    {
        public event UnhandledExceptionEventHandler ApplicationError;
        public delegate void ApplicationErrorEventHandler(ApplicationErrorArguments ex);
        public enum ApplicationSources
        {
            UI, CorePipeline, DB, Network, SQLite, Models
        }
        public void ApplicationErrorOccured(ApplicationErrorArguments ex)
        {

        }
    }
    public abstract class ErrorArguments
    {
        public Exception Exception { get; set; }
        public ErrorHandler.ApplicationSources Source { get; set; }
        public Dictionary<string, object> Data { get; set; }
        public bool Handled { get; set; }
        public DateTime Date { get; set; }
        public bool IsFatel { get; set; }
    }
    public class ApplicationErrorArguments : ErrorArguments
    {
       
       
    }
}
