﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency.Interfaces
{
    public interface IEnumerableDependencyEntry<T, TReturn> : IDependencyBaseEntry
    {
        IEnumerable<TReturn> ExecuteValues(T args);
        IAsyncResult ExecuteValuesAsync(T arg, AsyncCallback callback, object state);
    }
}
