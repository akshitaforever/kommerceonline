﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency.Interfaces
{

    public interface IValueDependencyEntry : IDependencyEntry
    {
        Object GetArgs();
        
    }
    public interface IValueReturnDependencyEntry : IValueDependencyEntry
    {

    }
    public interface IValueDependencyEntry<T> : IValueDependencyEntry
    {
        T ExecuteValue(params object[] args);
        IAsyncResult ExecuteValueAsync(AsyncCallback callback, object state, params object[] args);
    }
    public interface IValueDependencyEntry<T, TReturn> : IValueReturnDependencyEntry
    {
        TReturn ExecuteValue(T args);
        IAsyncResult ExecuteValueAsync(T arg, AsyncCallback callback, object state,params object[] args);

    }
    
}
