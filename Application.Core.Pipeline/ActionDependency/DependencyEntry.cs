﻿using Pipeline.ActionDependency.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency
{
    public class DependencyEntry : IDependencyEntry
    {
        private Task _task;
        private Guid _id;

        private Action DependencyAction { get; set; }

        public DependencyEntry()
        {
            _id = Guid.NewGuid();
        }
        public Guid ID
        {
            get
            {
                return _id;
            }

            set
            {
                throw new InvalidOperationException();
            }
        }

        public DependencyEntry(Action method):this()
        {
            this.DependencyAction = method;
        }
        public void Execute(params object[] args)
        {
            _task = new Task(DependencyAction);
            
        }

        public IAsyncResult ExecuteAsync(AsyncCallback callback, object state, params object[] args)
        {
            return DependencyAction.BeginInvoke(callback, state);
        }

        public System.Reflection.MethodInfo GetMethod()
        {
            return DependencyAction.Method;
        }

        public Task GetTask()
        {
            return _task;
        }

        public Task GetTask<T>()
        {
            return _task;
        }
    }

    public class DependencyEntry<T, TReturn> : IValueDependencyEntry<T, TReturn>
    {
        private Task<TReturn> _task;
        private Guid _id;

        public DependencyEntry()
        {
            _id = Guid.NewGuid();
        }
        public Func<T, TReturn> DependencyAction { get; set; }

        public Guid ID
        {
            get
            {
                return _id;
            }

            set
            {
                throw new InvalidOperationException();
            }
        }

        public TReturn ExecuteValue(T args)
        {
            return DependencyAction(args);
        }
        public IAsyncResult ExecuteValueAsync(T arg, AsyncCallback callback, object state, params object[] args)
        {
            return DependencyAction.BeginInvoke(arg, callback, state);
        }

        public void Execute(params object[] args)
        {
            _task = new Task<TReturn>(() => DependencyAction((T)args.First()));
            //  DependencyAction();
        }

        public IAsyncResult ExecuteAsync(AsyncCallback callback, object state, params object[] args)
        {
            throw new Exception();
            // return DependencyAction.BeginInvoke(arg, callback, state);
        }

        public object GetArgs()
        {
            return null;
        }

        public System.Reflection.MethodInfo GetMethod()
        {
            return DependencyAction.Method;
        }

        public Task GetTask()
        {
            return _task;
        }

        public Task GetTask<T1>()
        {
            return _task;
        }
    }
}
