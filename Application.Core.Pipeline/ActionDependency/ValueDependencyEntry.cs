﻿using Pipeline.ActionDependency.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency
{
    public class ValueDependencyEntry<T> : IValueDependencyEntry<T>
    {
        private Guid _id;
        public Func<T> DependencyAction { get; set; }

        public Guid ID
        {
            get
            {
                return _id;
            }

            set
            {
                throw new NotSupportedException();
            }
        }
        public ValueDependencyEntry()
        {
            _id = Guid.NewGuid();
        }
        public T ExecuteValue(params object[] args)
        {
            return DependencyAction.Invoke();
        }

        public IAsyncResult ExecuteValueAsync(AsyncCallback callback, object state, params object[] args)
        {
            return DependencyAction.BeginInvoke(callback, state);
        }
        public void Execute(params object[] args)
        {
            DependencyAction.Invoke();
        }

        public IAsyncResult ExecuteAsync(AsyncCallback callback, object state, params object[] args)
        {
            return DependencyAction.BeginInvoke(callback, state);
        }

        public object GetArgs()
        {
            return null;
        }

        public System.Reflection.MethodInfo GetMethod()
        {
            return DependencyAction.Method;
        }

        public Task GetTask()
        {
            throw new NotImplementedException();
        }

        public Task GetTask<T1>()
        {
            throw new NotImplementedException();
        }
    }
    public class ValueDependencyEntry<T1, T2> : IValueDependencyEntry<T1, T2>
    {
        private Guid _id;
        public Func<T1, T2> DependencyAction { get; set; }
        public T1 arg { get; set; }

        public Guid ID
        {
            get
            {
                return _id;
            }

            set
            {
                throw new NotSupportedException();
            }
        }
        public ValueDependencyEntry()
        {
            _id = Guid.NewGuid();
        }
        public T2 ExecuteValue(T1 arg)
        {
            return DependencyAction.Invoke(arg);
        }




        public IAsyncResult ExecuteValueAsync(T1 arg, AsyncCallback callback, object state, params object[] args)
        {
            return DependencyAction.BeginInvoke(arg, callback, state);
        }

        public void Execute(params object[] args)
        {
            throw new NotImplementedException();
        }

        public IAsyncResult ExecuteAsync(AsyncCallback callback, object state, params object[] args)
        {
            throw new NotImplementedException();
        }

        public object GetArgs()
        {
            return arg;
        }

        public System.Reflection.MethodInfo GetMethod()
        {
            return DependencyAction.Method;
        }

        public Task GetTask()
        {
            throw new NotImplementedException();
        }

        public Task GetTask<T>()
        {
            throw new NotImplementedException();
        }
    }
    public class ValueDependencyEntry : IValueDependencyEntry
    {
        public Func<Object> DependencyAction { get; set; }
        public Object arg { get; set; }
        private Guid _id;
        public ValueDependencyEntry()
        {
            _id = Guid.NewGuid();
        }
        public Guid ID
        {
            get
            {
                return _id;
            }

            set
            {
                throw new InvalidOperationException();
            }
        }

        public Object ExecuteValue(params object[] args)
        {
            return DependencyAction.Invoke();
        }

        public IAsyncResult ExecuteValueAsync(AsyncCallback callback, object state, params object[] args)
        {
            return DependencyAction.BeginInvoke(callback, state);
        }











        public void Execute(params object[] args)
        {
            DependencyAction.Invoke();
        }

        public IAsyncResult ExecuteAsync(AsyncCallback callback, object state, params object[] args)
        {
            return DependencyAction.BeginInvoke(callback, state);
        }

        public object GetArgs()
        {
            return arg;
        }

        public System.Reflection.MethodInfo GetMethod()
        {
            return DependencyAction.Method;
        }

        public Task GetTask()
        {
            throw new NotImplementedException();
        }

        public Task GetTask<T>()
        {
            throw new NotImplementedException();
        }
    }
}
