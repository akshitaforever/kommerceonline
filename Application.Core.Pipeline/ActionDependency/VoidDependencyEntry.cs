﻿using Pipeline.ActionDependency.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency
{
    public class VoidDependencyEntry:IDependencyEntry
    {
        public Action DependencyAction { get; set; }
        private Guid _id;
        public Guid ID
        {
            get
            {
                return _id;
            }

            set
            {
                throw new InvalidOperationException();
            }
        }

        public void Execute(params object[] args)
        {
             DependencyAction.Invoke();
        }

        public IAsyncResult ExecuteAsync(AsyncCallback callback, object state, params object[] args)
        {
            return DependencyAction.BeginInvoke(callback, state);
        }
        public System.Reflection.MethodInfo GetMethod()
        {
            return DependencyAction.Method;
        }

        public Task GetTask()
        {
            throw new NotImplementedException();
        }

        public Task GetTask<T>()
        {
            throw new NotImplementedException();
        }
    }
}
