﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency
{
    public class ObjectLock
    {
        public bool IsActive { get; set; }
        public Pipeline.ActionDependency.ObjectEntry.ObjectLockType LockType { get; set; }
    }
}
