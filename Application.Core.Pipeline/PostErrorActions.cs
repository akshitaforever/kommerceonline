﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline
{
    public class PostErrorActions<T, TEx> where TEx : ErrorArguments 
    {
        private List<Func<T,TEx ,bool>> _PostErrorActions = new List<Func<T,TEx, bool>>();

    }
    public abstract class GenericAction
    {
      public abstract  bool Process();
    }

}
