﻿using ConfigurationManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            DefaultApplicationBootstrapper bootstrapper = new DefaultApplicationBootstrapper();

            bootstrapper.Configure();
        }
    }
}
