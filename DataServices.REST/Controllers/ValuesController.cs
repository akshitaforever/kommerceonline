﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DataServices.REST.Controllers
{
    /// <summary>
    /// Sample Controller
    /// </summary>
  //  [Authorize]
    
    public class ValuesController : ApiController
    {
        /// <summary>
        /// s
        /// </summary>
        /// <returns></returns>
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

      
    }
}
