using System.Web.Http;
using WebActivatorEx;
using DataServices.REST;
using Swashbuckle.MVC.Handler;
using Swashbuckle.MVC;
using Swashbuckle.Application;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
[assembly: PreApplicationStartMethod(typeof(SwaggerMVCConfig), "Register")]
namespace DataServices.REST
{
    public class SwaggerMVCConfig
    {
        public static void Register()
        {
            DynamicModuleUtility.RegisterModule(typeof(SwashbuckleMVCModule));
            GlobalConfiguration.Configuration
  .EnableSwagger(c => c.SingleApiVersion("v1", "KommerceOnline REST API"))
  .EnableSwaggerUi();
        }
    }
}