﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationFramework.DI
{
    public class ContainerEntry
    {
        protected Type TConcrete { get; set; }
        protected Type TImplementation { get; set; }
        protected Object _instance = null;
        protected bool _singleton;
        protected bool _isstatic;
        private Func<Object, Object> _method;
        private Object _methodargs = null;
        private ITypeResolver _resolver = null;
        private Object _resolverargs = null;
        private bool _generic = false;
        public bool IsGeneric { get { return _generic; } }
        public Type ImplementationType { get { return this.TImplementation; } }
        public Type ConcreteType { get { return this.TConcrete; } }

        public ContainerEntry(Type TConcrete, Type TImplementation)
        {
            this.TConcrete = TConcrete;
            this.TImplementation = TImplementation;
        }
        public ContainerEntry(Type TConcrete, Type TImplementation, bool generic)
        {
            this.TImplementation = TImplementation;
            this.TConcrete = TConcrete;
            _generic = true;
        }
        public ContainerEntry AsStatic<TImplementation>(TImplementation instance) where TImplementation : class
        {
            if (instance.GetType() == typeof(TImplementation))
            {
                _isstatic = true;
                this._instance = instance;
                return this;
            }
            throw new InvalidOperationException();
        }
        public ContainerEntry AsSingleton()
        {
            _singleton = true;
            return this;
        }
        public ContainerEntry ConstructUsing(Func<Object, Object> method, Object args)
        {
            _method = method;
            this._methodargs = args;
            return this;
        }
        public ContainerEntry ConstructUsingTypeResolver(ITypeResolver resolver, Object args)
        {
            _resolver = resolver;
            this._resolverargs = args;
            return this;
        }
        public TImplementation GetInstance<TImplementation>() where TImplementation : class
        {
            if (_singleton)
            {

                this._instance = this._instance ?? ResolveInstance<TImplementation>();
                return this._instance as TImplementation;
            }
            if (_isstatic)
            {
                return this._instance as TImplementation;
            }
            return ResolveInstance<TImplementation>();
        }
        private TImplementation ResolveInstance<TImplementation>() where TImplementation : class
        {
            Type targettype;
            if (this._resolver != null)
            {
                targettype = this._resolver.Resolve(this._resolverargs);
                return GetObject(targettype) as TImplementation;
            }
            return GetObject<TImplementation>();

        }
        private Object ResolveInstance(params Type[] genericargs)
        {
            Type targettype;
            if (this._resolver != null)
            {
                targettype = this._resolver.Resolve(this._resolverargs);
                return GetObject(targettype);
            }
            return GetObject(genericargs);

        }
        private TImplementation GetObject<TImplementation>() where TImplementation : class
        {
            if (this._method != null)
                return _method(_methodargs) as TImplementation;
            if (TConcrete.IsConstructedGenericType)
            {
                if (_generic)
                {
                    //var instance=Activator.CreateInstance(this.TConcrete.GetGenericTypeDefinition().MakeGenericType(this.TImplementation.GenericTypeArguments));
                    var instance = Activator.CreateInstance(this.TConcrete.GetGenericTypeDefinition().MakeGenericType(typeof(TImplementation).GenericTypeArguments[0]));
                    return instance as TImplementation;
                }
                else
                    return Activator.CreateInstance(this.TConcrete.GetGenericTypeDefinition().MakeGenericType(this.TImplementation.GenericTypeArguments)) as TImplementation;
            }
            return Activator.CreateInstance(TConcrete) as TImplementation;
        }

        private Object GetObject(params Type[] genericargs)
        {
            if (this._method != null)
                return Convert.ChangeType(_method(_methodargs), TImplementation);
            if (TConcrete.IsConstructedGenericType)
            {
                if (TConcrete.IsConstructedGenericType)
                {
                    if (_generic)
                    {
                        
                        //var instance=Activator.CreateInstance(this.TConcrete.GetGenericTypeDefinition().MakeGenericType(this.TImplementation.GenericTypeArguments));
                        var instance = Activator.CreateInstance(this.TConcrete.GetGenericTypeDefinition().MakeGenericType(genericargs[0].GetGenericArguments()[0]));
                        return instance;
                    }
                    else
                        return Activator.CreateInstance(this.TConcrete.GetGenericTypeDefinition().MakeGenericType(genericargs));
                }

            }
            return Activator.CreateInstance(TConcrete);
        }
        private Object GetObject(Type type)
        {

            return Activator.CreateInstance(type);
        }
        public Object GetInstance(params Type[] genericargs)
        {

            if (_singleton)
            {
                this._instance = this._instance ?? ResolveInstance();
            }
            return ResolveInstance(genericargs);

        }
    }
}
