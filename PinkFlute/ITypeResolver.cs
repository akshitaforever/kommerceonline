﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationFramework.DI
{
    public interface ITypeResolver
    {
        Type Resolve(Object parameter);
    }
}
