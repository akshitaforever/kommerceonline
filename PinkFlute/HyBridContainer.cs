﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AutomationFramework.DI
{
    public class HyBridContainer : IServiceProvider
    {
        internal Dictionary<Type, ContainerEntry> _maptypes = new Dictionary<Type, ContainerEntry>();
        public ContainerEntry Register<TConcrete, TImplementation>() where TConcrete : class
        {
            ContainerEntry entry = new ContainerEntry(typeof(TImplementation), typeof(TConcrete));
            _maptypes.Add(typeof(TConcrete), entry);
            return entry;
        }
     
        public ContainerEntry RegisterGenericWithBase<TConcrete, TImplementation>() where TConcrete : class
        {
            ContainerEntry entry = new ContainerEntry(typeof(TImplementation),typeof(TConcrete),true);
            _maptypes.Add(typeof(TConcrete).GetGenericTypeDefinition(), entry);
            return entry;
        }
        private ContainerEntry GetEntry(Type serviceType)
        {

            if (_maptypes.ContainsKey((serviceType)))
            {
                return _maptypes[serviceType];
            }

            //
            var entry = (_maptypes.Values.FirstOrDefault(p =>
            {
                if (serviceType.IsGenericTypeDefinition)
                {
                    var timpl = serviceType.GetGenericTypeDefinition();
                    if (!p.IsGeneric)
                        return false;
                    var i = p.ConcreteType.GetGenericTypeDefinition().GetInterfaces();
                    var intrfce = p.ConcreteType.GetGenericTypeDefinition().GetInterface(timpl.Name);
                    if (intrfce != null)
                    {
                        return intrfce.GUID == timpl.GUID;
                    }
                    return false;
                }
                return false;
            }));
            return entry;

        }
        public TImplementation GetInstance<TImplementation>() where TImplementation : class
        {

            var entry = GetEntry(typeof(TImplementation));
            if (entry != null)
            {
                return entry.GetInstance<TImplementation>();
            }
            return null;
        }


        public object GetService(Type serviceType)
        {
            var entry = GetEntry(serviceType);
            if (entry != null)
                return entry.GetInstance();
            return null;
        }
    }




}
