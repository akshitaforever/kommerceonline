﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AutomationFramework.DI
{
    public class InjectedControllerFactory : DefaultControllerFactory
    {
        public override IController CreateController(System.Web.Routing.RequestContext requestContext, string controllerName)
        {
            string controllername = requestContext.RouteData.Values["controller"].ToString();
            Type controllerType = Assembly.GetCallingAssembly().DefinedTypes.SingleOrDefault(p => p.GetInterface("IController") != null && p.Name == controllername);

            IController controller = Activator.CreateInstance(controllerType) as IController;
            return controller;

        }
    }
}
