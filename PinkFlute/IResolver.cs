﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AutomationFramework.DI
{
    public interface IResolver
    {
        TImplementation GetInstance<TImplementation>() where TImplementation : class;
      
    }
}
