﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AutomationFramework.DI
{
    public class AspNetMvcResolver : IResolver, IDependencyResolver
    {
        HyBridContainer container=null;
        public AspNetMvcResolver(HyBridContainer container)
        {
            this.container = container;
        }
        public AspNetMvcResolver()
        {

        }
        

        public object GetService(Type serviceType)
        {
            if (!serviceType.IsAbstract && typeof(IController).IsAssignableFrom(serviceType))
            {
                return this.container.GetService(serviceType);
            }
            
            return ((IServiceProvider)this.container).GetService(serviceType);
          
        }
        
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return new Object[] { GetService(serviceType) };
        }

        public TImplementation GetInstance<TImplementation>() where TImplementation : class
        {
            return container.GetInstance<TImplementation>();
        }
    }
}