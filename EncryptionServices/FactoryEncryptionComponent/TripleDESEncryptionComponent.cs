﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
namespace EncryptionServices.FactoryEncryptionComponent
{
    public class TripleDESEncryptionComponent<T> : AbstractEncryptionComponent<T> where T : class
    {
        private TripleDES _instance;
        private const int BLOCK_SIZE = 16;//16 Bytes
        public TripleDESEncryptionComponent(byte[] key, byte[] IV)
        {
            _instance = TripleDES.Create();
            _instance.IV = IV;
            _instance.Key = key;

        }
        public int BlockSize
        {
            get
            {
                return _instance.BlockSize;
            }
            set
            {
                this._instance.BlockSize = value;

            }
        }



        public override ICryptoTransform GetEncryptor()
        {
            return _instance.CreateEncryptor();

        }

        public override ICryptoTransform GetDecryptor()
        {
            return _instance.CreateDecryptor();
        }
        public override byte[] GenerateKey()
        {
            _instance.GenerateKey();
            return _instance.Key;
        }

        public override byte[] GenerateIV()
        {
            _instance.GenerateIV();
            return _instance.IV;
        }
    }
}
