﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoNet;
using System.Security.Cryptography;
namespace EncryptionServices
{
    public class BlowFishEncryptionComponent<T>:AbstractEncryptionComponent<T> where T:class
    {

        private Blowfish _instance;
        private const int BLOCK_SIZE = 16;//16 Bytes
        public BlowFishEncryptionComponent(byte[] key, byte[] IV)
        {
            
            _instance = new Blowfish();
            _instance.IV = IV;
            _instance.Key = key;
            _instance.BlockSize = 32;
            
        }
        public override ICryptoTransform GetEncryptor()
        {
            return _instance.CreateEncryptor();
        }

        public override ICryptoTransform GetDecryptor()
        {
            return _instance.CreateDecryptor();
        }
        public override byte[] GenerateKey()
        {
            _instance.GenerateKey();
            return _instance.Key;
        }

        public override byte[] GenerateIV()
        {
            _instance.GenerateIV();
            return _instance.IV;
        }
    }
}
