﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
namespace EncryptionServices.Hashing
{
    public class SHAHashingModule<T> : IHashingModule<T> where T : class
    {
        SHA1Managed shObj = new SHA1Managed();
        public SHAHashingModule()
        {
            shObj.Initialize();
        }
        public string ComputeHash(T obj)
        {
            return Convert.ToBase64String(ComputeHashToBytes(obj));
        }

        public byte[] ComputeHashToBytes(T obj)
        {
            return shObj.ComputeHash(StreamUtilities.ObjectToByteArray<T>(obj));
        }

    }
}
