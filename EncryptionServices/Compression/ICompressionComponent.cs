﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncryptionServices.Compression
{
    public interface ICompressionComponent<T> where T:class
    {
        byte[] Compress(T obj);
        T DeCompress(byte[] rawdata);
    }
}
