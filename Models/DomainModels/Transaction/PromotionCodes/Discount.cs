﻿using Models.DomainModels.FactoryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.Transaction.PromotionCodes
{
    public class DiscountModel:SqlBaseModel
    {
        public double Value { get; set; }
    }
}
