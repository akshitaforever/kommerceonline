﻿using Models.DomainModels.FactoryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.Transaction
{
    public class TaxStack : SqlBaseModel
    {
        public double Value { get; set; }
        public string Expression { get; set; }
        public string Parameters { get; set; }

    }
    public class TaxOverrideStack: TaxStack
    {
        public Enums.TaxType Type { get; set; }
    }
}
