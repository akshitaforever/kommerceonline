﻿using Models.DomainModels.FactoryModels;
using Models.DomainModels.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.Transaction.Orders
{
    public class ProductTransaction : SqlBaseModel
    {
        public ProductModel Product { get; set; }
        public int Quantity { get; set; }
        public Enums.Unit Unit { get; set; }
        public double Value { get; set; }

    }
}
