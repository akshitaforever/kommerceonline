﻿using Models.DomainModels.FactoryModels;
using Models.DomainModels.Person;
using Models.DomainModels.Product;
using Models.DomainModels.Transaction.PromotionCodes;
using Models.DomainModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.Transaction.Orders
{
    public class OrderModel : SqlBaseModel
    {
        public string OrderID { get; set; }
        public DateTime Date { get; set; }
        public string DeliveryNote { get; set; }
        public string SupplierRef { get; set; }
        public string OtherReferences { get; set; }
        public string TermsOfDelivery { get; set; }
        public PaymentInfo PaymentInfo { get; set; }
        public EndUser User { get; set; }
        public ICollection<ProductTransaction> Products { get; set; }
        public AddressModel Address { get; set; }
        public double TotalValue { get; set; }
        public ICollection<TaxStack> Taxes { get; set; }
        public double FinalValue { get; set; }
        public ICollection<DiscountModel> Discount { get; set; }
    }
}
