﻿using Models.DomainModels.FactoryModels;

namespace Models.DomainModels.Transaction.Orders
{
    public class PaymentInfo:SqlBaseModel
    {
        public Enums.PaymentMode PaymentMode { get; set; }
        public string OtherInfo { get; set; }
        public string CardSuffix { get; set; }
    }
}