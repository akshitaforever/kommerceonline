﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels
{
    public class Enums
    {
        public enum Unit
        {
            Nos, Kg
        }
        public enum TaxType
        {
            ItemTotal, GrandTotal, Surcharge
        }
        public enum PaymentMode
        {
            Cash, Cheque, CreditCard, WireTransfer, Other, DebitCard
        }
        public enum ReportType
        {
            Invoice, Audit, Stock, Transaction
        }
    }
}
