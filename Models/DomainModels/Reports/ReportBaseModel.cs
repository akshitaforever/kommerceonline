﻿using Models.DomainModels.FactoryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.Reports
{
    public class ReportBaseModel : SqlBaseModel
    {
        public ICollection<ReportElement> Elements { get; set; }
        public Enums.ReportType ReportType { get; set; }
        public bool Enabled { get; set; }
    }
}
