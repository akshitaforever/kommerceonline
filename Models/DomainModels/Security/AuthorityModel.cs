﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.Security
{
    public enum AuthorityLevel
    {
        /*
        Level 0-Maximum Level-All Access to All Modules
        Level 1-Full Access to particualr Module,Audit
        Level 2-Full Access to particular Entity
        Level 3-Database Access to Entity and User Data
        Level 4-Entity Deletion Previlege
        Level 5-Entity UpdationPrevilege
        Level 6-Entity Add Previlege
        Level 7-Minimum-Read Only Access to Particualr Entity
            */
        SuperAdmin, Admin, PowerUser, DatabaseOperators, DatabaseInsertOnly, DatabaseUpdateOnly, DatabaseDeleteOnly, DatabaseViewOnly
    }
    public class AuthorityModel
    {
    }
}
