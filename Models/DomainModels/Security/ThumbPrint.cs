﻿using Models.DomainModels.FactoryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.Security
{
    public class ThumbPrint : SqlBaseModel
    {
        public byte[] Data { get; set; }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
