﻿using Models.DomainModels.FactoryModels;

namespace Models.DomainModels.License
{
    public class Declaration : SqlBaseModel
    {
        public string DeclarationString { get; set; }
    }
}