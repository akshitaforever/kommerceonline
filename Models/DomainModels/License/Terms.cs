﻿using Models.DomainModels.FactoryModels;

namespace Models.DomainModels.License
{
    public class Terms : SqlBaseModel
    {
        public string TermsString { get; set; }
    }
}