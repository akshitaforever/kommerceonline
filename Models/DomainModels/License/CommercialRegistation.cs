﻿using Models.DomainModels.FactoryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.License
{
    public class CommercialRegistation : SqlBaseModel
    {
        public string Description { get; set; }
        public string FirmName { get; set; }
        public string Value { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }

    }
}
