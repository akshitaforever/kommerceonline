﻿using Models.DomainModels.FactoryModels;
using Models.DomainModels.Person;
using Models.DomainModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.License
{
    public class LicenseInformationModel : SqlBaseModel
    {
        public OrganizationalUser Licensee { get; set; }
        public ICollection<AddressModel> Addresses { get; set; }
        public ICollection<PhoneContact> Phones { get; set; }
        public ICollection<EmailContact> Emails { get; set; }
        public byte[] LicenseCrypto { get; set; }
        public string LicenseeFirmName { get; set; }
        public ICollection<CommercialRegistation> FirmRegistationInfo { get; set; }
        public ICollection<Terms> TermsAndConditions { get; set; }
        public ICollection<Declaration> Declarations { get; set; }
    }
}
