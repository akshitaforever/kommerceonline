﻿using Models.DomainModels.FactoryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.Common
{
    public class SeriesGenerator:SqlBaseModel
    {      
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public string FormatString { get; set; }
        public string Domain { get; set; }
    }
}
