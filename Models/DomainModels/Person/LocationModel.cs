﻿using Models.DomainModels.FactoryModels;

namespace Models.DomainModels.Person
{
    public class LocationModel: SqlBaseModel
    {
        public string Location { get; set; }
        public GeoData GeographicalData { get; set; }
        public string Zip { get; set; }

    }
}