﻿using Models.DomainModels.FactoryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.Person
{
    public class PersonModel:SqlBaseModel
    {
        public ProperName FullName { get; set; }
        public ICollection<PhoneContact> Phones { get; set; }
        public EmailContact Email { get; set; }
        public ICollection<AddressModel> Addresses { get; set; }
    }
}
