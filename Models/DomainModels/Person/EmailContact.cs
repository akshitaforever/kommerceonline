﻿using Models.DomainModels.FactoryModels;

namespace Models.DomainModels.Person
{
    public class EmailContact:BaseModel
    {
        public string Email { get; set; }
        public ProperName Name { get; set; }
    }
}