﻿using Models.DomainModels.FactoryModels;

namespace Models.DomainModels.Person
{
    public class PhoneContact:SqlBaseModel
    {
        public PhoneType Type { get; set; }
        public bool IsPrimary { get; set; }
        public string Number { get; set; }
        public string Prefix { get; set; }
        public string Extention { get; set; }
    }
}