﻿using Models.DomainModels.FactoryModels;

namespace Models.DomainModels.Person
{
    public class GeoData:SqlBaseModel
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}