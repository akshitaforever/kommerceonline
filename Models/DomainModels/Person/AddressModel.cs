﻿using Models.DomainModels.FactoryModels;

namespace Models.DomainModels.Person
{
    public class AddressModel : SqlBaseModel
    {
        public string StreetAddress { get; set; }
        public string ContactName { get; set; }
        public PhoneContact ContactNumber { get; set; }
        public string ApartmentNumber { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string SpecialInstructions { get; set; }
        public string Notes { get; set; }
        public GeoData LocationData { get; set; }
    }
}