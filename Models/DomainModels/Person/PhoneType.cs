﻿namespace Models.DomainModels.Person
{
    public enum PhoneType
    {
        Landline, Mobile, Fax, Pager,Other
    }
}