﻿using Models.DomainModels.FactoryModels;
using Models.DomainModels.Person;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.Product
{
    public class ProductModel : SqlBaseModel
    {
        public string Description { get; set; }
        public string[] Images { get; set; }
        public double UnitPrice { get; set; }
        public double ShippingPrice { get; set; }
        public ManufacturerModel Manufacturer { get; set; }
        public LocationModel Location { get; set; }
        public string SKU { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }

    }
}
