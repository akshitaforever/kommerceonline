﻿using Models.DomainModels.FactoryModels;
using Models.DomainModels.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.Product
{
    public class ManufacturerModel : SqlBaseModel
    {
        public string Address { get; set; }
        public ICollection<PhoneContact> Phones { get; set; }
        public ICollection<EmailContact> Emails { get; set; }
        public string[] Website { get; set; }
        public string OtherInfo { get; set; }
    }
}
