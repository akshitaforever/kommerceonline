﻿using Models.DomainModels.FactoryModels;
using Models.DomainModels.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.User.Profile
{
    public class WishlistModel : SqlBaseModel
    {
        public ProductModel Product { get; set; }

    }
}
