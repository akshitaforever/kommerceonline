﻿using Models.DomainModels.FactoryModels;
using Models.DomainModels.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.User.Profile
{
    public class EndUserProfile : SqlBaseModel
    {
        public WishlistModel Wishlist { get; set; }

    }
}
