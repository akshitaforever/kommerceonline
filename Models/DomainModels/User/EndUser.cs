﻿using Models.DomainModels.Person;
using Models.DomainModels.User.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.User
{
    public class EndUser : UserModel
    {
        public EndUserProfile Profile { get; set; }
    }
}
