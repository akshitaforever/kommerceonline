﻿using Microsoft.AspNet.Identity.EntityFramework;
using Models.DomainModels.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.User
{
    public class UserModel : IdentityUser
    {
        public ProperName Name { get; set; }
        public PersonModel Person { get; set; }
        public Guid UID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid ModifiedBy { get; set; }
        public AccountStatus Status { get; set; }
    }
}
