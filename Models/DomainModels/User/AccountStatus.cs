﻿namespace Models.DomainModels.User
{
    public enum AccountStatus
    {
        Raw, Active, Inactive, Disabled, Violated
    }
}