﻿using Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.FactoryModels
{
    public class MongoBaseModel : BaseModel, IDistinguishedModel
    {
        /// <summary>
        /// ID for Mongo Databases.Not Required if you are not using Mongo
        /// </summary>
        public new ObjectId IID { get { return (ObjectId)base.ID; } set { base.ID = value; } }

        public string Name
        {
            get; set;
        }

        public Guid ModuleID
        {
            get; set;
        }

        public MongoBaseModel()
        {
            IID = ObjectId.GenerateNewId();
        }

        public object GetID()
        {
            return IID;
        }

        public void SetID(object ID)
        {
            this.IID = (ObjectId)ID;
        }
    }
}
