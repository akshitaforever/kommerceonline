﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.FactoryModels
{
    public interface IDistinguishedModel
    {
        string Name { get; set; }
        Guid ModuleID { get; set; }
        object GetID();
        void SetID(object ID);
    }
}
