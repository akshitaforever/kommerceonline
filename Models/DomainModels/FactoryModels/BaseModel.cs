﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.FactoryModels
{
    public class BaseModel
    {
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedData { get; set; }
        public Guid ModifiedBy { get; set; }
        public object  ID { get; set; }

    }
}
