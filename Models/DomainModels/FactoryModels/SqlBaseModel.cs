﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DomainModels.FactoryModels
{
    public class SqlBaseModel : BaseModel, IDistinguishedModel
    {
       

        public Guid ModuleID
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }
        [Key]
        public Guid ID
        {
            get { return (Guid)base.ID; }
            set { base.ID = value; }
        }

        public object GetID()
        {
            return ID;
        }

        public void SetID(object ID)
        {
            this.ID = (Guid)ID;
        }
    }
}
